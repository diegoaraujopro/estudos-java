package br.com.alura.parte6;

import estudo.ContaCorrente;

/*
 * Array de Referências
 */
public class Arrays2 {

	public static void main(String[] args) {
		
		ContaCorrente[] contas = new ContaCorrente[5];
		
		ContaCorrente cc1 = new ContaCorrente(2727, 15666);
		ContaCorrente cc2 = new ContaCorrente(2828, 43635);
		
		contas[0] = cc1;
		contas[1] = cc2;
		
		System.out.println(contas[0].getNumero());
		System.out.println(contas[1].getNumero());

	}

}
