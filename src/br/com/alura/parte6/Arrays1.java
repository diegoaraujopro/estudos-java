package br.com.alura.parte6;

/*
 * Array de primitivos
 */
public class Arrays1 {
	
	public static void main(String[] args) {
		int[] idades = new int[5];

		for (int i = 0; i < idades.length; i++) {
			idades[i] = i * i;
			System.out.println("Idade " + i + " é: " + idades[i]);
		}

	}

}
