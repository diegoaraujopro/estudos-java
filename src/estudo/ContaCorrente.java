package estudo;

public class ContaCorrente extends Conta {

	private String numero;
	private String agencia;
	
	public ContaCorrente() {
		
	}
	
	public ContaCorrente(int agencia, int numero) {
		this.agencia = Integer.toString(agencia);
		this.numero = Integer.toString(numero);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

}
