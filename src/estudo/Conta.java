package estudo;

public class Conta {

	private Long id;
	private double saldo;
	private String nome;
	private Conta children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Conta getChildren() {
		return children;
	}

	public void setChildren(Conta children) {
		this.children = children;
	}
}
