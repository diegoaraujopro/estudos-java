package estudo;

import java.util.HashMap;
import java.util.Map;

public class TestaMapa {
	
	public static void main (String args []) {
		Conta c1 = new ContaCorrente();
		c1.setId(1l);
		c1.setSaldo(4050.9);
		c1.setNome("Diego");
		
		Conta c2 = new ContaCorrente();
		c2.setId(2l);
		c2.setSaldo(78050.4);
		c2.setNome("Ana Paula");
		
		Conta c3 = new ContaCorrente();
		c3.setId(2l);
		c3.setSaldo(98050.4);
		c3.setNome("Mariana");
		
		// cria o mapa
		Map<String, Conta> mapaContas = new HashMap<>();
		
		mapaContas.put("funcionario", c1);
		mapaContas.put("gerente", c2);
		
		// qual a conta do gerente?
		
		Conta contaGerente = mapaContas.get("gerente");
		System.out.println("O nome do gerente é: " + contaGerente.getNome());
		
		c1.setChildren(c3);
		
		c2.setChildren(c1);
		
		// c2.getChildren().setChildren(c1);
		
		
		System.out.println("Filho do filho: " + c2.getChildren().getChildren().getNome());
		
	}

}
